<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \App\Http\Middleware\ApiAuthMiddleware;


Route::get('/', function () {
    return view('welcome');
});


//RUTAS DE API

//Route::get('/usuario/pruebas', 'UserController@pruebas');
//Route::get('/categoria/pruebas', 'CategoryController@pruebas');
//Route::get('/post/pruebas', 'PostController@pruebas');


//RUTAS DE CONTROLLADOR DE USUARIO

Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@login');
Route::put('/api/user/update', 'UserController@update');
Route::post('/api/user/upload', 'UserController@upload')->middleware(ApiAuthMiddleware::class);

Route::get('/api/user/avatar/{filename}', 'UserController@getImagen');

Route::get('/api/user/profile/{id}', 'UserController@profile');



//RUTAS DE CONTROLLADOR DE CATEGORIAS

Route::resource('/api/category', 'CategoryController');

//RUTAS DE CONTROLLADOR DE POST

Route::resource('/api/post', 'PostController');
Route::post('/api/post/upload', 'PostController@upload');
Route::get('/api/post/image/{filename}', 'PostController@getImagen');
Route::get('/api/post/category/{id}', 'PostController@getPostByCategory');
Route::get('/api/post/user/{id}', 'PostController@getPostByUser');
