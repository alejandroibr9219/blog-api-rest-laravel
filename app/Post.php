<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{



    protected $table ='posts';

      protected $fillable = [
        'title', 'content', 'category_id', 'image'
    ];
    //RELACION DE MUCHOS A UNO
    public function user(){
    	return $this->belongsto('App\User', 'user_id');
    }

      public function category(){
    	return $this->belongsto('App\Category', 'category_id');
    }
}

