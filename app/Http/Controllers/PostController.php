<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;
use App\Helpers\JwtAuth;



class PostController extends Controller
{
    public function __construct(){
        $this->middleware('api.auth', ['except' => ['index', 'show', 'getImagen','getPostByCategory', 'getPostByUser']]); 
    }



   public function index(){

     	$posts = Post::all();

     	return response()->json([

     		'code' => '200',
     		'status'=> 'success',
     		'post'=> $posts
     	]);
  	
    }   

     public function show($id){

    	$post = Post::find($id)->load('category')->load('user');

    	if(is_object($post)){

    		$data = array(
    			'status' => 'success',
    			'code'  => '200',
    			'posts'    => $post
    		);

    	}else{

    		$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'El post no existe',
    			
    	);
    	}

    	return response()-> json($data, $data['code']);
    }

 public function store(Request $request){

    		$json = $request->input('json', null);
    		$params = json_decode($json);
    		$params_array = json_decode($json, true);
    		
    	

    	if(!empty($params) && !empty($params_array)){
    	 
    	
    	$user = $this->getIdentity($request);
    	
    	//Validar usuario
    		$validate = \Validator::make($params_array, [

    			'title' 		=> 'required',
    			'content' 		=> 'required',
    			'category_id' 		=> 'required',
    			'image'         =>   'required'

    			
    		]);

    		if($validate->fails()){

    		$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'El post no se ha guardado faltan datos',
    			
    	);
    		}else{
    

    	//Crear el suuario
            $post = new Post();
            $post->user_id = $user->sub;
            $post->category_id = $params->category_id;
            $post->title = $params->title;
            $post->content = $params->content;  
            $post->image = $params->image; 
 				
 				
                //guardar el usuario
            $post->save();

    		$data = array(
    			'status' => 'success',
    			'code'  => '200',
    			'post'    => $post
    		);


    	}
    	}else{


    			$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'Los Datos enviados no son correcto'
    			
    	);
    	}
    	
    


    	return response()-> json($data, $data['code']);


    }

 public function update($id, Request $request){

 	$user = $this->getIdentity($request);


 	$json = $request->input('json', null);
    $params_array = json_decode($json, true);
    
  $data = array(
                
                'code'  => 404,
                'status' => 'error',
                'message' => 'Datos enviados incorrectamente'
        );

    if(!empty($params_array)){

       
        //Validar Datos
            $validate = \Validator::make($params_array, [

    			'title' 		=> 'required',
    			'content' 		=> 'required',
    			'category_id' 		=> 'required'
    			

    			
    		]);

	if($validate->fails()){
		$data['errors'] = $validate->errors();
		return response()->json($data, $data['code']);


	}
         //Quitar los campos que no quiere actualizar

            unset($params_array['id']);
            unset($params_array['user_id']);
            unset($params_array['created_at']);
            unset($params_array['user']);

        //Conseguir usuario Identificado
         $user = $this->getIdentity($request);

        //Buscar el registro

  $post = Post::where('id', $id)
   					->where('user_id', $user->sub)->first();
   			if(!empty($post) && is_object($post)){

    //Actualizar Usuario en la Bd
   				$post->update($params_array);

   	//Devolver algo
   				 $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'post'   => $post,
                'changes' => $params_array
               
        );

   			}

       /*  $where = [
         	'id' => $id,
         	'user_id' => $user->sub
         ];
         $post = Post::updateOrCreate($where, $params_array);*/

        //Devolver array con resultado
          
    }
  return response()->json($data, $data['code']);


    }


   public function destroy($id, Request $request){

   //conseguir usuario identificado
   	$user = $this->getIdentity($request);
 

   	//conseguir el registro
   $post = Post::where('id', $id)
   					->where('user_id', $user->sub)->first();


   	if(!empty($post))
   	{
   		$post->delete();

   		  $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'post'   => $post,
               
               
        );
   	  }else{
   	  		$data = array(
                
                'code'  => 404,
                'status' => 'error',
                'message' => 'El post no existe'
        );

   	  }	  
   		 return response()->json($data, $data['code']);
   }

private function getIdentity($request){

	$JwtAuth = new JwtAuth();
    $token = $request->header('Authorization', null);	
    $user = $JwtAuth->checkToken($token, true);

    return $user;

	}

 public function upload(Request $request){

            //Recogemos los Datos
            $image = $request->file('file0');


            //Validacion de Imagen 
             $validacion = \Validator::make($request->all(), [

                'file0'  => 'required|image|mimes:jpg,jpeg,png,gif'
                
            ]);


            //Guardar Imagen
            if(!$image || $validacion->fails()){
                  $data = array(
                
                'code'  => 400,
                'status' => 'error',
                'message' => 'La imagen no se ha Subido'
        );

            }else{
                 
                  $image_name = time().$image->getClientOriginalName();
                \Storage::disk('images')->put($image_name, \File::get($image));

                 $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'image' => $image_name
               
        );
              
            }

         
    return response()->json($data, $data['code']);

  }


 public function getImagen($filename){

    $isset = \Storage::Disk('images')->exists($filename);
    if($isset){

  $file = \Storage::Disk('images')->get($filename); return new Response($file, 200);

  }else{
          $data = array(
                
                'code'  => 404,
                'status' => 'error',
                'message' => 'Imagen no existe'
               
        );

        }
    return response()->json($data, $data['code']);

    }

    public function getPostByCategory($id){

    	$posts = Post::where('category_id', $id)->get();

    	return response()->json([

                'status' => 'success',
                'posts' => $posts
    	], 200);
    }

 public function getPostByUser($id){

    	$posts = Post::where('user_id', $id)->get();

    	return response()->json([

                'status' => 'success',
                'posts' => $posts
    	], 200);
    }

}
