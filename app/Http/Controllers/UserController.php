<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller
{
    public function pruebas(Request $Request){

    	return "Accion de pruebas de user controller";
    }


    public function register(Request $request){
    	//Recoger datos del usuario por Post
    		$json = $request->input('json', null);
    		$params_array = json_decode($json, true);
    		$params = json_decode($json);
    	

    	if(!empty($params) && !empty($params_array)){
    	//Limpiar Datos
    	$params_array = array_map('trim', $params_array);	
    	
    	//Validar usuario
    		$validate = \Validator::make($params_array, [

    			'name' 		=> 'required|alpha',
    			'surname' 	=> 'required|alpha',
    			'email' 	=> 'required|email|unique:users',//Comprobar si el ususario existe
    			'password' 		=> 'required'
    		]);

    		if($validate->fails()){

    		$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'El usuario no se ha creado',
    			'errors' => $validate->errors()
    	);
    		}else{
    	//Cifrar Contraseña
    		$pwd = hash('sha256', $params -> password);

    	


    	//Crear el suuario
                $user = new User();
                $user->name     = $params_array['name'];
                $user->surname  = $params_array['surname'];
                $user->email    = $params_array['email'];
                $user->password = $pwd;
                $user->role     = 'ROLE_USER';
 				
 				
                //guardar el usuario
 
               $user->save();

    		$data = array(
    			'status' => 'success',
    			'code'  => '200',
    			'message' => 'El usuario  se ha creado correctamente',
    			'user'    => $user
    		);


    	}
    	}else{


    			$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'Los Datos enviados no son correcto',
    			
    	);
    	}
    	
    


    	return response()-> json($data, $data['code']);


    }

     public function login(Request $request){
     		
            $JwtAuth = new \JwtAuth();

            //Recibir datos pos post
            $json = $request->input('json', null);
            $params_array = json_decode($json, true);
            $params = json_decode($json);
            //Valiar esos datos
            $validate = \Validator::make($params_array, [

              
                'email'     => 'required|email',//Comprobar si el ususario existe
                'password'      => 'required'
            ]);

            if($validate->fails()){
                //La validacion a fallado
            $signup = array(
                'status' => 'error',
                'code'  => '404',
                'message' => 'El usuario no se ha logueado',
                'errors' => $validate->errors()
        );
            }else{
            //Cifrar password
            $pwd = hash('sha256', $params -> password);
            //Devolver todos esos datos
            $signup = $JwtAuth->signup($params->email, $pwd);

            if(!empty($params->gettoken)){

                $signup = $JwtAuth->signup($params->email, $pwd, true);
            }
}
           
            
            return response()->json($signup,200);
    }

  public function update(Request $request){
    //Comprobar que el usuario este identificado
    $token = $request->header('Authorization');
    $JwtAuth = new \JwtAuth();
    $checkToken = $JwtAuth->checkToken($token);

     //Recogemos datos por post
    $json = $request->input('json', null);
    $params_array = json_decode($json, true);
    
    if($checkToken && !empty($params_array)){

        //Sacar Usuario Identificado
        $user = $JwtAuth->checkToken($token, true);
       
        //Validar Datos
            $validate = \Validator::make($params_array, [

                'name'      => 'required|alpha',
                'surname'   => 'required|alpha',
                'email'     => 'required|email|unique:users'.$user->sub

            ]);

         //Quitar los campos que no quiere actualizar

            unset($params_array['id']);
            unset($params_array['role']);
            unset($params_array['password']);
            unset($params_array['created_at']);
            unset($params_array['remember_token']);

        //Actualizar Usuario en la Bd

            $user_update = User::where('id', $user->sub)->update($params_array);

        //Devolver array con resultado
           $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'user' => $user,
                'changes' => $params_array
        );
    }else{
        $data = array(
                
                'code'  => 400,
                'status' => 'error',
                'message' => 'El usuario no se ha identificado'
        );
   }
  return response()->json($data, $data['code']);

  }  

  public function upload(Request $request){

            //Recogemos los Datos
            $image = $request->file('file0');


            //Validacion de Imagen 
             $validacion = \Validator::make($request->all(), [

                'file0'  => 'required|image|mimes:jpg,jpeg,png,gif'
                
            ]);


            //Guardar Imagen
            if(!$image || $validacion->fails()){
                  $data = array(
                
                'code'  => 400,
                'status' => 'error',
                'message' => 'La imagen no se ha Subido'
        );

            }else{
                 
                  $image_name = time().$image->getClientOriginalName();
                \Storage::disk('users')->put($image_name, \File::get($image));

                 $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'image' => $image_name
               
        );
              
            }

         
    return response()->json($data, $data['code']);

  }

  public function getImagen($filename){

    $isset = \Storage::Disk('users')->exists($filename);
    if($isset){

  $file = \Storage::Disk('users')->get($filename); return new Response($file, 200);

  }else{
          $data = array(
                
                'code'  => 404,
                'status' => 'error',
                'message' => 'Imagen no existe'
               
        );

        }
    return response()->json($data, $data['code']);

    }

public function profile($id){

    $user = User::find($id);

    if(is_object($user)){
          $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'user' => $user
               
        );  

    }else{

           $data = array(
                
                'code'  => 404,
                'status' => 'error',
                'message' => 'El usuario no existe'
               
        );
    }
    return response()->json($data, $data['code']);

  }
}
