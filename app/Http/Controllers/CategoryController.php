<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Category;

class CategoryController extends Controller
{
    
	 public function __construct(){
        $this->middleware('api.auth', ['except' => ['index', 'show']]); 
    }


     public function index(){

     	$categories = Category::all();

     	return response()->json([

     		'code' => '200',
     		'status'=> 'success',
     		'categories'=> $categories
     	]);
  	
    }

    public function show($id){

    	$category = Category::find($id);

    	if(is_object($category)){

    		$data = array(
    			'status' => 'success',
    			'code'  => '200',
    			'category'    => $category
    		);

    	}else{

    		$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'La categoria no existe',
    			
    	);
    	}

    	return response()-> json($data, $data['code']);
    }

    public function store(Request $request){

    		$json = $request->input('json', null);
    		$params_array = json_decode($json, true);


    	if(!empty($params_array)){
    	
    	
    	//Validar usuario
    		$validate = \Validator::make($params_array, [

    			'name' 		=> 'required',
    			
    		]);

    		if($validate->fails()){

    		$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'La categoria no se ha creado',
    			
    	);
    		}else{
    

    	//Crear el suuario
                $category = new Category();
                $category->name     = $params_array['name'];
               
 				
 				
                //guardar el usuario
 
               $category->save();

    		$data = array(
    			'status' => 'success',
    			'code'  => '200',
    			'message' => 'La categoria se ha enviado correctamente',
    			'category'    => $category
    		);


    	}
    	}else{


    			$data = array(
    			'status' => 'error',
    			'code'  => '404',
    			'message' => 'la categoria no se guardado correctamente',
    		
    	);
    	}
    	
    


    	return response()-> json($data, $data['code']);


    }


    public function update($id, Request $request){

 	$json = $request->input('json', null);
    $params_array = json_decode($json, true);
    
    if(!empty($params_array)){

       
        //Validar Datos
            $validate = \Validator::make($params_array, [

                'name'      => 'required',
                
            ]);

         //Quitar los campos que no quiere actualizar

            unset($params_array['id']);
            unset($params_array['created_at']);

        //Actualizar Usuario en la Bd

            $category = Category::where('id', $id)->update($params_array);

        //Devolver array con resultado
           $data = array(
                
                'code'  => 200,
                'status' => 'success',
                'category' => $params_array
               
        );
    }else{
        $data = array(
                
                'code'  => 400,
                'status' => 'error',
                'message' => 'La categoria no se ha actualizado'
        );
   }
  return response()->json($data, $data['code']);


    }
}
